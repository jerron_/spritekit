//
//  GameScene.swift
//  SprikitIntro
//
//  Created by GDD Student on 26/5/16.
//  Copyright (c) 2016 OMTA. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
   
    var cannon:SKSpriteNode!
    var touchLocation:CGPoint = CGPointZero
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
       cannon = self.childNodeWithName("cannon") as! SKSpriteNode
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {

        touchLocation = touches.first!.locationInNode(self)
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        let percent = touchLocation.x / size.width
        let newAngle = percent * 180 - 180
        cannon.zRotation = CGFloat(newAngle) * CGFloat(M_PI) / 180.0
        
        
    }
}
